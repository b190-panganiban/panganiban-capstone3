import {useContext} from 'react';
import {Navbar, Container, Nav} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar(props) {
	
	const {user} = useContext(UserContext);

	console.log(user);

	return (

		<Navbar bg="navbar navbar-dark bg-dark navbar-style" expand="lg">
		  <Container>
		    <Navbar.Brand as={Link} to="/">EYECONIC STUDIOS</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">

		      	{
		      		(user.isAdmin) ?
		      		<Nav.Link as={NavLink} to="/admin" exact>Admin Control Page</Nav.Link>
		      		:
		      		(user.isAdmin === false) ?
		      		<>
		      		<Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
			        <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
			        <Nav.Link as={NavLink} to="/cart" exact>Cart</Nav.Link>
			        <Nav.Link as={NavLink} to="/history" exact>History</Nav.Link>
			        </>
		      		:
		      		<>
		      		<Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
			        <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
			        </>
		      	}
		        
		        
		        {
		        (user.id !== null) ?

		        <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
		        
		        
		        :
		        <>
		        <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
		        
		        <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
		        </>
		        
		    	}
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

	)
}