import {Card, Button, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Products ({productProp}) {
	
	const {name, description, price, stocks, _id} = productProp

	console.log(productProp);

	console.log(name);

	return (
			
			<Card className="p-0 mt-3">
			  <Card.Header as="h5">{name}</Card.Header>
			  <Card.Body>
			    <Card.Title>Description</Card.Title>
			    <Card.Text>
			      {description}
			    </Card.Text>
			    <Card.Text>Price: {price}</Card.Text>
				<Card.Text>Available: {stocks}</Card.Text>
			    <Button variant="primary" as={Link} to={`/products/${_id}`}>See Details</Button>
			  </Card.Body>
			</Card>
	)
}