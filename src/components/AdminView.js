import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import {useState, useEffect} from 'react'
import Swal from 'sweetalert2';
 

export default function AdminView({allProductProp}){
	
	const {name, description, price, stocks, isActive, _id} = allProductProp;
	console.log(isActive)

	const [toggle, setToggle] = useState(isActive);


	function deactivate(){
		console.log(_id);

		fetch(`https://rocky-garden-36310.herokuapp.com/products/${_id}/archive`, {
			method: 'PUT',
			headers: {
				"Content-type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);


			if(data === true){
				Swal.fire({
					title: `${name} is Deactivated`,
					icon: "success",
				})

				setToggle(toggle => !toggle);
			}
			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please contact admin"
				})
			}

		})
	}

	function activate(){
		console.log(_id);

		fetch(`https://rocky-garden-36310.herokuapp.com/products/${_id}/activate`, {
			method: 'PUT',
			headers: {
				"Content-type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);


			if(data === true){
				Swal.fire({
					title: `${name} is Activated`,
					icon: "success",
				})

				setToggle(toggle => !toggle);
			
			}
			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please contact admin"
				})
			}

		})
	}

	return (

		    <tr>
		      <td></td>
		      <td>{name}</td>
		      <td>{description}</td>
		      <td>{price}</td>
		      <td>{stocks}</td>
		   
		      <th><Link className="btn btn-primary" to={`/update/${_id}`}>Update</Link></th>

		      {
		      	(toggle) ? 
		      		<th><Button className="btn btn-danger" onClick={deactivate}>Deactivate</Button></th>
		      	:
		      		<th><Button className="btn btn-success" onClick={activate}>Activate</Button></th>
		      }
		    </tr>
	)
}