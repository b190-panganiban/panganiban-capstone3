import {useState, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';


export default function Login() {
	
	const history = useNavigate();

	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');



	function authenticate(e) {
		e.preventDefault();

		fetch('https://rocky-garden-36310.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);

				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to EYECONIC STUDIOS'
				})



			}
			else {
				Swal.fire({
					title: 'Authorization Failed',
					icon: 'error',
					text: 'Please check your credentials'
				})
			}


		})
		.catch(err => console.log(err))
	}

	const retrieveUserDetails = (token) => {
		fetch('https://rocky-garden-36310.herokuapp.com/users/details', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

			console.log(user)

			if(data.isAdmin){
				history('/admin')
			}
			else {
				history('/')
			}
		})
	}

	return (
		(user.isAdmin === true)?
		<Navigate to="/admin" />
		:
		(user.id !== null) ?
		<Navigate to="/products" />
		:
		<Form onSubmit={e => authenticate(e)}>
			
			<h1>Login</h1>

			<Form.Group conterolId="email">
				<Form.Label>Email</Form.Label>
				<Form.Control 
					type = "text"
					placeholder = "Email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type = "password"
					placeholder = "Password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			<Button className="mt-3" variant="primary" id="loginBtn" type="submit">Login</Button>
		</Form>

	)
}