import Carousel from 'react-bootstrap/Carousel';

export default function Home () {
	return (
		<Carousel variant="dark">
		  <Carousel.Item>
		    <img
		      className="d-block"
		      style={{height: 800}}
		      src="https://mir-s3-cdn-cf.behance.net/project_modules/1400_opt_1/e2583f88186561.5dce7505260ab.jpg"
		      alt="First slide"
		    />

		    <Carousel.Caption>
		      <h3>First slide label</h3>
		      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block"
		      style={{height: 800}}
		      src="https://mir-s3-cdn-cf.behance.net/project_modules/1400_opt_1/89505d88186561.5dce750524b1d.jpg"
		      alt="Second slide"
		    />

		    <Carousel.Caption>
		      <h3>Second slide label</h3>
		      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block"
		      style={{height: 800}}
		      src="https://mir-s3-cdn-cf.behance.net/project_modules/1400_opt_1/34e6ff88186561.5dce75051b8bc.jpg"
		      alt="Third slide"
		    />

		    <Carousel.Caption>
		      <h3>Third slide label</h3>
		      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>

		)
}