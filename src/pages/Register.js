import { Form, Button } from 'react-bootstrap';
import {useState} from 'react';
import { useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register () {


	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	console.log(email);
	console.log(password);

	function registerUser(e) {
		e.preventDefault();

		fetch('https://rocky-garden-36310.herokuapp.com/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: 'Email Already Exists',
					icon: 'error',
					text: 'Please provide another email.'
				})
			}
			else {
				fetch('https://rocky-garden-36310.herokuapp.com/users/register',{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data === true){
						setFirstName('');
						setLastName('');
						setEmail('');
						setPassword('');
						setMobileNo('');

						Swal.fire({
							title: 'Registration successful',
							icon: 'success',
							text: 'Welcome to EYECONIC STUDIOS'
						})

						history('/login')
					}

					else {
						Swal.fire({
							title: 'Something went wrong',
							icon: 'error',
							text: 'Please try again'
						})
					}
				})
			}
		})

		
	}


	return (
		
		<Form onSubmit={e => registerUser(e)}>
			<h1>Register</h1>



			<Form.Group className="mt-3" controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type = "text"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					placeholder = "Input your First Name here"
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type = "text"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					placeholder = "Input your Last Name here"
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="email">
				<Form.Label>Email</Form.Label>
				<Form.Control 
					type = "email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					placeholder = "Input your Email here"
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type = "password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					placeholder = "Input your Password here"
					required
				/>
			</Form.Group>

			<Form.Group className="mt-3" controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type = "number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					placeholder = "Input your Mobile Number here"
					required
				/>
			</Form.Group>

			<Button className="mt-3" variant="primary" type="submit" id="submitBtn">Submit</Button>

		</Form>
		

	)
} 