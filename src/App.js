import './App.css';

import { UserProvider } from './UserContext'


import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login'
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Cart from './pages/Cart'
import CreateProduct from './pages/CreateProduct'
import Update from './pages/Update'
import OrderHistory from './pages/OrderHistory';
import Error from './pages/Error';
import Admin from './pages/Admin';



function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

    fetch('https://rocky-garden-36310.herokuapp.com/users/details', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
          <Container>
              <Routes>
              {
                (user.isAdmin) ?
                <>
                <Route exact path="/register" element={<Error/>} />
                <Route exact path="/products" element={<Error />} />
                <Route exact path="/*" element={<Error />} />
                <Route exact path="/create" element={<CreateProduct/>} />
                <Route exact path="/update/:productId" element={<Update/>} />
                <Route exact path="/admin" element={<Admin/>}/>
                <Route exact path="/Login" element={<Error/>}/>
                <Route exact path="/Logout" element={<Logout/>}/>
                </>
                :
                <>
                <Route exact path="/" element={<Home/>}/>
                <Route exact path="/admin" element={<Error/>}/>
                <Route exact path="/register" element={<Register/>} />
                <Route exact path="/products" element={<Products />} />
                <Route exact path="/products/:productId" element={<ProductView/>} />
                <Route exact path="/cart" element={<Cart />} />
                <Route exact path="/history" element={<OrderHistory />} />
                <Route exact path="/Login" element={<Login/>}/>
                <Route exact path="/Logout" element={<Logout/>}/>
                <Route exact path="/*" element={<Error />} />
                </>
              }
              
            </Routes>
          </Container>
      </Router>
      </UserProvider>
  )
}

export default App;
